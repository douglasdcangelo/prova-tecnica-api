package br.com.sicredi.steps;

import static io.restassured.RestAssured.given;
import org.junit.Assert;

import com.google.gson.Gson;

import br.com.sicredi.request.SimulationResquestBody;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;

public class SimulacaoSteps {
	private String base = "http://localhost:8080";
	Response response;
	Boolean tf = false;
	SimulationResquestBody simulationResquestBody = new SimulationResquestBody();

	@When("List simulations")
	public void list_simulations1() {
		response = given().get(base + "/api/v1/simulacoes");
		System.out.println("lista simulação" + response.asString());
	}

	@Given("that doesnt have any simulatios")
	public void that_doesnt_have_any_simulatios() {
		
		response = given().get(base + "/api/v1/simulacoes");
		java.util.List<Object> t = response.getBody().jsonPath().getList("id");
		for (Object i : t) {
			delete_a_simulation(i.toString());
		}

	}

	@Given("A CPF that has a simulation")
	public void cpf_whith_simulation() {

		response = given().get(base + "/api/v1/simulacoes");
		System.out.println(response.asString());
		if (response.getBody().jsonPath().getString("cpf").contains("02703854567")) {
			System.out.println("contem");

		} else {
			create_Joao();
		}

	}

	@When("list simulation by CPF")
	public void list_simulation_by_CPF() {
		response = given().get(base + "/api/v1/simulacoes/02703854567");
		Assert.assertEquals(200, response.getStatusCode());
	}

	public void create_Joao() {
		simulationResquestBody = new SimulationResquestBody("02703854567", "João ", "joa@gmail.com", "3000.00", "4",
				"true", null);
		newSimulation();
	}

	@Then("Simulation Status code is {int}")
	public void simulation_status_code_is(int int1) {
		Assert.assertEquals(int1, response.getStatusCode());

	}

	@Then("Response is {string}")
	public void response_is(String string) {
		// take a look
		System.out.println("resposta " + response.getBody().jsonPath().get().toString());
		System.out.println("Body  " + simulationResquestBody.toString());
		Assert.assertTrue(response.getBody().jsonPath().get().toString().contains(string));

	}

	@Given("{string} {string} {string} {string} {string} {string}")
	public void creatNewSimulation(String cpf, String name, String mail, String value, String parcela,
			String security) {

		simulationResquestBody = new SimulationResquestBody(cpf, name, mail, value, parcela, security, null);
	}

	@When("Creat a new simulation")
	public void newSimulation() {
		System.out.println(simulationResquestBody);
		response = given().log().all().contentType("application/json").body(simulationResquestBody).when()
				.post(base + "/api/v1/simulacoes");

	}

	@When("Create New Simulations for the same CPF")
	public void create_new_simulations_for_the_same_cpf() {
		newSimulation();
		System.out.println(simulationResquestBody);
		if (response.getStatusCode() == 201) {
			newSimulation();
			response = given().log().all().contentType("application/json").body(simulationResquestBody).when()
					.post(base + "/api/v1/simulacoes");

		}

	}

	@Then("the same data inserted as return")
	public void the_same_data_inserted_as_return() {

		Gson j = new Gson();
		SimulationResquestBody s = j.fromJson(response.getBody().asString(), SimulationResquestBody.class);
		Assert.assertEquals(s.toString(), simulationResquestBody.toString());

	}

	@Given("I pass a {string} that has a simulation")
	public void i_pass_a_that_has_a_simulation(String id) {
		simulationResquestBody.setId(id);

	}

	@Given("I pass a {string} that dont exist")
	public void i_giv_id_that_dont_exist(String id) {
		simulationResquestBody.setId(id);

	}

	@When("I Delete a simulation")
	public void i_delete_a_simulation() {
		delete_a_simulation(simulationResquestBody.getId());

	}

	@When("Edit a simulation {string}")
	public void edit_a_simulation(String cpf) {
		create_Joao();

		simulationResquestBody = new SimulationResquestBody("02703854567", "João ", "joa@gmail.com", "3000.00", "4",
				"true", null);

		response = given().log().all().contentType("application/json").body(simulationResquestBody).when()
				.put(base + "/api/v1/simulacoes/" + cpf);
	}

	public void delete_a_simulation(String id) {
		response = given().delete(base + "/api/v1/simulacoes/" + id);

	}

}

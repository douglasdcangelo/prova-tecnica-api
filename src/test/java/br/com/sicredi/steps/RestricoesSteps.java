package br.com.sicredi.steps;

import org.junit.Assert;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import static io.restassured.RestAssured.given;

public class RestricoesSteps {
	String base = "http://localhost:8080";
	Response response;
	String cpf;

	@Given("cpf {string}")
	public void cpf(String string) {

		cpf = string;

	}

	@When("i consult a cpf")
	public void i_consult_a_cpf() {

		response = given().get("http://localhost:8080/api/v1/restricoes/" + cpf);

	}

	@Then("response is {string} {string}")
	public void response_is(String string, String string2) {

		String r = string + cpf + string2;

		Assert.assertEquals(r, response.getBody().jsonPath().get("mensagem"));
	}

	@Then("Status code is {int}")
	public void status_code_is(int int1) {

		Assert.assertEquals(response.getStatusCode(), int1);

	}

}

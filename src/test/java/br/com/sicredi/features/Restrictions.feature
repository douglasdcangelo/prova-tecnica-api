@ok
Feature: Restrictions on CPF
	when i try make a cpf consult
	and i pass a cpf 
	then i hope see  if i have or not a restriction 

  Scenario Outline: CPF with restriction
    Given cpf "<input_cpf>"
    When i consult a cpf
		Then Status code is 200
		And response is "O CPF " " tem problema"
#		And response is "O CPF {cpf} possui restrição"
		
		Examples: 
      | input_cpf |
      |97093236014|
      |60094146012|
      |84809766080|
      |62648716050|
      |26276298085|
      |01317496094|
      |55856777050|
      |19626829001|
      |24094592008|
      |58063164083|
      
  Scenario Outline: CPF without restriction
    Given cpf "<input_cpf>"
    When i consult a cpf
    Then Status code is 204
    
    Examples: 
     |input_cpf  |
     |12344444312|
     |12344455667|
     |02039447890|


#		i probaly will do this test"
  #Scenario: Incorrect CPF ?
    #Given CPF 1234
    #When i consult a cpf
    #Then Status code is 205

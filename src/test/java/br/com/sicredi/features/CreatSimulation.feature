
Feature: Creat Simulations
@ok
Scenario Outline: Create New Simulations with sucess
		Given "<Cpf_input>" "<Name_input>" "<Mail_input>" "<Value_input>" "<Parcela_input>" "<Seguro_input>"
		When Creat a new simulation
		Then Simulation Status code is 201
	  And the same data inserted as return  
	  Examples:
		|Cpf_input			|Name_input		|Mail_input								|Value_input|Parcela_input|Seguro_input	|
		|020381248272354|Douglas A		|douglasangelo@gmail.com	|2.000			|3						|true					|
	@ok
	Scenario Outline: Create New Simulations without some information
		Given "<Cpf_input>" "<Name_input>" "<Mail_input>" "<Value_input>" "<Parcela_input>" "<Seguro_input>"
		When Creat a new simulation
	
		And Response is "<Message_out>"
		Then  Simulation Status code is 400
		
		
		#				está retonando 201 no sucesso e seria 400
	  
	  
	  Examples:
	|	Cpf_input		|Name_input	|Mail_input								|Value_input	|Parcela_input|Seguro_input	|Message_out							|
	|							|Douglas A	|douglasangelo@gmail.com	|2.000				|3						|true					|CPF não pode ser vazio|
	|027038112339	|						|douglasangelo@gmail.com	|2.000				|3						|true					|Nome não pode ser vazio		|
	|027038113238	|Douglas A	|													|2.000				|3						|true					|E-mail não deve ser vazio	|
	|027403811237	|Douglas A	|douglasangelo@gmail.com	|							|3						|true					|Valor não pode ser vazio		|
	|027035811236	|Douglas A	|douglasangelo@gmail.com	|		2.000			|							|true					|Parcelas não pode ser vazio|
	|027023811235	|Douglas A	|douglasangelo@gmail.com	|2.000				|		3					|							|Seguro não pode ser vazio	|
	|027023811234	|Douglas A	|douglasangelo@gmail.com	|2.000				|		1					|			true		|Parcelas deve ser igual ou maior que 2|
	|027023811233	|Douglas A	|douglasangelo@gmail.com	|2.000				|		49				|			true		|Parcelas deve ser igual ou maior que 48|
	|027023811232	|Douglas A	|douglasangelo@gmail.com	|40.001				|		49				|			true		|Valor deve ser igual ou menor que 40.000|
	|027023811231	|Douglas A	|douglasangelo@gmail.com	|500					|		3					|			true		|Valor deve ser igual ou maior que 1.000|
	|027023811231	|Douglas A	|douglasangelo						|2.000				|		3					|			true		|E-mail deve ser um e-mail válido|
	|027.023.811-31|Douglas A	|douglasangelo@gmail.com	|2.000				|		3					|			true		|CPF deve estar em um formato válido|
	
	
	
	  @ok

	Scenario Outline: Create New Simulations for the same CPF 
		Given "<Cpf_input>" "<Name_input>" "<Mail_input>" "<Value_input>" "<Parcela_input>" "<Seguro_input>"
		When Create New Simulations for the same CPF  
		Then  Simulation Status code is 400
	  And Response is "<Message_out>"
		#	 mensagem  CPF duplicado but shuld be CPF já existente
	  
	Examples:
	|Cpf_input			|Name_input		|Mail_input								|Value_input	|Parcela_input|Seguro_input	|Message_out|
	|020381248272354|Douglas A		|douglasangelo@gmail.com	|2.000				|3						|true					|CPF duplicado|
	
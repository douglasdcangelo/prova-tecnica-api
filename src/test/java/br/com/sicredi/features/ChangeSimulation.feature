Feature: Change Simulations
	@ok
	Scenario Outline: Edit  Simulations with a CPF that dont exist
		Given "09328442" "joao silva" "mail@gmail.com" "3.000" "5" "true"
		When Edit a simulation "60094146013"
		And Response is "CPF 60094146013 não encontrado"
		Then  Simulation Status code is 404
	
@ok
	Scenario Outline: Edit  Simulations
		Given "<Cpf_input>" "<Name_input>" "<Mail_input>" "<Value_input>" "<Parcela_input>" "<Seguro_input>"
		When Edit a simulation "<Cpf_input>"
		And the same data inserted as return
		Then Simulation Status code is 200
		
	  Examples:
	|	Cpf_input		|Name_input	|Mail_input								|Value_input	|Parcela_input|Seguro_input	|Message_out							|
	|	02703854567	|Douglas A	|douglasangelo@gmail.com	|2.000				|3						|true					|CPF não pode ser vazio|
	
	
	
	@ok
		Scenario Outline: Edit  Simulations with problem
		Given "<Cpf_input>" "<Name_input>" "<Mail_input>" "<Value_input>" "<Parcela_input>" "<Seguro_input>"
		When Edit a simulation "02703854567"
		And Response is "<Message_out>"
		Then  Simulation Status code is 404
		
	  Examples:
	|	Cpf_input		|Name_input	|Mail_input								|Value_input	|Parcela_input|Seguro_input	|Message_out							|
	|							|Douglas A	|douglasangelo@gmail.com	|2.000				|3						|true					|CPF não pode ser vazio|
	|02703854567	|						|douglasangelo@gmail.com	|2.000				|3						|true					|Nome não pode ser vazio		|
	|02703854567	|Douglas A	|													|2.000				|3						|true					|E-mail não deve ser vazio	|
	|02703854567	|Douglas A	|douglasangelo@gmail.com	|							|3						|true					|Valor não pode ser vazio		|
	|02703854567	|Douglas A	|douglasangelo@gmail.com	|2.000			|							|true					|Parcelas não pode ser vazio|
	|02703854567	|Douglas A	|douglasangelo@gmail.com	|2.000				|		3					|							|Seguro não pode ser vazio	|
	|02703854567	|Douglas A	|douglasangelo@gmail.com	|2.000				|		1					|			true		|Parcelas deve ser igual ou maior que 2|
	|02703854567	|Douglas A	|douglasangelo@gmail.com	|2.000				|		49				|			true		|Parcelas deve ser igual ou maior que 48|
	|02703854567	|Douglas A	|douglasangelo@gmail.com	|40.001				|		49				|			true		|Valor deve ser igual ou menor que 40.000|
	|02703854567	|Douglas A	|douglasangelo@gmail.com	|500					|		3					|			true		|Valor deve ser igual ou maior que 1.000|
	|02703854567	|Douglas A	|douglasangelo						|2.000				|		3					|			true		|E-mail deve ser um e-mail válido|
	|02703854567	|Douglas A	|douglasangelo@gmail.com	|2.000				|		3					|			true		|CPF deve estar em um formato válido|
	
package br.com.sicredi.main;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = "pretty", monochrome = true, tags = "@ok",
		glue ="br/com/sicredi/steps",
		features = "src/test/java/br/com/sicredi/features", 
		dryRun = false)
public class RunCucumberTest {

}
